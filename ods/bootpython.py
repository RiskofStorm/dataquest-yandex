
# MODEL/FILE SAVE/LOAD
import pickle
from google.colab import drive, files


drive.mount('/content/gdrive')

class StormColab:
    
    '''
    Saves and loads files and pickle dumps from ~/Colab Notebooks folder !only!
    Chosen only folder with .ipynb for sake of simpliness 

    METHOD      PARAMETERS

    model_save  (model, file_name: str, params:dict=None, annotation: str=None) 
    model_load  (file_name: str) 
    file_save   (file_name: str) 
    file_load   (file_name: str) 

    '''

    @staticmethod
    def model_save(model, file_name: str, params:dict=None, annot: str=None) -> None:
        data_path = '/content/gdrive/My Drive/Colab Notebooks/' + file_name
        data_to_save = [model, params, annot]

        with open(data_path, 'wb') as f:
            pickle.dump(data_to_save, f)
        print(f'saved at location {data_path}')

    @staticmethod
    def model_load(file_name: str) -> None:
        file_name = '/content/gdrive/My Drive/Colab Notebooks/' + file_name
        with open(file_name,'rb+') as f:
            return pickle.load(f)

    @staticmethod
    def file_save(file_name: str) -> None:
        data_path = '/content/gdrive/My Drive/Colab Notebooks/' + file_name
        with open(file_name, 'r+') as f_to_save:
            with open(data_path, 'w+') as f_to_write:
                f_to_write.write(f_to_save.read())
        print(f'saved at location {data_path}')

    @staticmethod
    def file_load(file_name: str) -> None:
        data_path = '/content/gdrive/My Drive/Colab Notebooks/' + file_name
        with open(data_path, 'r+') as f:
            with open(file_name, 'w+') as wf:
                wf.write(f.read())
        print(f'loaded file {file_name}')


# Yandex RandomForest GradientBoosting algorithm
# CATBOOST SHAP
try:
    from catboost import CatBoostClassifier, Pool, cv
    import shap
except ModuleNotFoundError:
    !pip install shap  --quiet
    !pip install catboost --quiet
    from catboost import CatBoostClassifier, Pool, cv
    import shap

from google.colab import drive
drive.mount('/content/gdrive')


# KAGGLE CREDENTIALS
import os 
__kaggle_username = ""
# !!! CATION
## REMOVE BEFORE PUSH TO GIT!
__kaggle_key = ""
os.environ['KAGGLE_USERNAME'] = __kaggle_username
os.environ['KAGGLE_KEY'] = __kaggle_key


def submit_kaggle(sample_sub, name='flight_pred.csv'):
    sample_sub = pd.read_csv('sample_submission.csv', index_col='id')
    sample_sub['dep_delayed_15min'] = ctb_test_pred
    sample_sub.to_csv(name)
    print(f"{name} writed")
    
# extract all zip-files into working dir
def extract_all_zip() -> None:
    from zipfile import ZipFile
    files = list()
    print('Extrated:')
    for f in os.listdir():
        if f.endswith('.zip'):
            files.append(f)
            ZipFile(f, 'r').extractall()
            print(f)        
    print(f'Extraction complete at location {os.getcwd()}')
