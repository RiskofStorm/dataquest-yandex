
class view_dataframe_stats:

    def __init__(self, df):
        self.df = df
        self.cols = df.columns
        self.unique_vals = [df[col].unique() for col in self.cols]

    def view_unique_vals(self): 
        view_df = pd.DataFrame.from_dict(dict(zip(self.cols, self.unique_vals)),
                                         orient='index')
        view_df.fillna(" ", inplace=True)
        return view_df.T
        
    def unique_len(self):
        unique_len = [len(val) for val in self.unique_vals]
        return dict(zip(self.cols, unique_len))


def time_class(time):
    if  500 <= time < 1200:
        return 'morning'
    elif 1200 <= time < 1700:
        return 'afternoon'
    elif 1700 <= time < 2100:
        return 'evening'
    return 'night' 

def season_class(month):
    if 3 <= month < 6:
        return 'spring'
    elif 6 <= month < 9:
        return 'summer'
    elif 9 <= month < 12:
        return 'autumn'
    return 'winter'

def distance_class(distance):
    if distance < 150:
        return 'short'
    elif 150 <= distance < 317:
        return 'middle'
    elif 317 <= distance < 575:
        return 'long'
    elif 575 <= distance:
        return 'extreme long'
